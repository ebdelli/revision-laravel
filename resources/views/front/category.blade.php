@extends('layouts.front')

@section('content')
    <div class="container">
        @foreach($posts as $p)
            <div class="row">
                <div class="col-sm-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">{{ $p->title }}</div>
                        <div class="panel-body">
                            {{ $p->content }}
                        </div>
                    </div>
                </div>
            </div>
        @endforeach
    </div>
@stop