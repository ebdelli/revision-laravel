Création d'un projet "revision"
composer create-project --prefer-dist laravel/laravel revision

Si APP_KEY dans .env est vide:
php artisan key:generate

Faire les migrations par défaut (table users)
php artisan migrate

Ajouter les système d'authentification
php artisan make:auth

Créer un modèle avec la migration
php artisan make:model Models/Category -m

Créer un middleware
php artisan make:middleware IsAdmin

Créer un controlleur resource
php artisan make:controller Admin/CategoriesController --resource

Créer une requete de validation
php artisan make:request AddPostRequest

Lancer le serveur
php artisan serve

Après téléchargement de l'atelier n'oublier pas de créer le fichier .env a partir de .env.example (il suffit de le copier et regénérer l'APP_KEY)
Et installer les dépendances : composer install