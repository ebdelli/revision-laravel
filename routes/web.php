<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your application. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/

Route::get('/', ['as' => 'index', 'uses' => 'FrontController@index']);
Route::get('/category/{id}', ['as' => 'category', 'uses' => 'FrontController@category']);

Auth::routes();

Route::get('/home', 'HomeController@index');

Route::group(['prefix' => 'admin', 'namespace' => 'Admin', 'middleware' => ['auth', 'auth.admin']], function(){
    Route::resource('categories', 'CategoriesController');
    Route::resource('posts', 'PostsController');
});
